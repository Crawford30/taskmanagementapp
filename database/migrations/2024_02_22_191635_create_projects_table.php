<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->enum('status', ['awaiting-start', 'in-progress', 'on-hold', 'completed'])->default('awaiting-start');
            $table->unsignedBigInteger('engineer_id');
            $table->unsignedBigInteger('project_manager_id');
            $table->timestamps();

            //====Constraints
            $table->foreign('engineer_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('project_manager_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('projects');
    }
};
