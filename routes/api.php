<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\MilestoneController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});



Route::prefix('projects')->group(function () {
    Route::post('/create-project', [ProjectController::class, 'create']);
    Route::put('/{id}/status', [ProjectController::class, 'changeStatus']);
    Route::post('/{projectId}/milestones', [MilestoneController::class, 'create']);
    Route::get('/engineer', [ProjectController::class, 'listProjectsAssignedToEngineer']);
    Route::put('/{project}/change-engineer', [ProjectController::class, 'changeEngineer']);
    Route::put('/{project}/mark-completed', [ProjectController::class, 'markProjectAsCompleted']);
});



