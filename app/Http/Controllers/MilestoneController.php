<?php

namespace App\Http\Controllers;

use App\Models\Milestone;
use Illuminate\Http\Request;

class MilestoneController extends Controller
{
    public function create(Request $request, $projectId)
    {
        $milestone = Milestone::create([
            'name' => $request->input('name'),
            'status' => $request->input('status'),
            'project_id' => $projectId,
        ]);

        return response()->json($milestone, 201);
    }
}
