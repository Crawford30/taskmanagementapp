<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProjectController extends Controller
{


public function listOfProjectsProjectsAssignedToEngineer()
{

    $projects = auth()->user()->projects()->get();

    return response()->json(['projects' => $projects], 200);
}


    public function create(Request $request)
    {
        $project = Project::create($request->all());
        return response()->json($project, 201);
    }

    public function changeStatus(Request $request, $id)
    {
        $project = Project::findOrFail($id);

        // ===========Only allow changing status if not already completed=======
        if ($project->status !== 'completed') {
            $project->update(['status' => $request->input('status')]);
            return response()->json($project, 200);
        }

        return response()->json(['error' => 'Project is already completed. Status cannot be changed.'], 400);
    }



//====Change Engineer =====
 public function changeEngineer(Project $project)
{

    $newEngineerId = request('new_engineer_id');

    $project->update(['engineer_id' => $newEngineerId]);

    return response()->json(['message' => 'Engineer changed successfully'], 200);
}



//=====Mark Project as Completed
public function markProjectAsCompleted(Project $project)
{
    $project->update(['status' => 'completed']);

    return response()->json(['message' => 'Project marked as completed'], 200);
}


}
